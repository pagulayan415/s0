//  *** ARRAYS ***

// let movies = ["Godfather", "Mission Impossible", "Avegners", "Venom", "The Nun"];

// console.log(movies[0]); //Godfather
// console.log(movies[1]); //Mission Impossible
// console.log(movies[2]); //Avengers

// movies[1] = "Halloween"

// console.log(movies[1]); //Halloween
// console.log(movies.length); //length is 5

// //We can initialize an empty array two ways:
// let colors = []
// let colors = new Array() // uncommon


// //Arrays can hold any type of data

// let random_collection = [49, true, "Hermione", null];

// let nums = [45, 37, 89, 24];
// nums.length //4

// let colors = ["red", "orange", "yellow"];
// // console.log(colors); // output is ['red', 'orange', 'yellow']
// // colors.push("green")
// // console.log(colors); // output is ['red', 'orange', 'yellow', 'green']

// // colors.pop(); //removed yellow
// // console.log(colors);

// colors.unshift("infrared");//['infrared', 'red', 'orange', 'yellow']
// console.log(colors);

// console.log(colors.shift());//shift returns the removed item
// console.log(colors);

// let tuitt = ["Charles", "Paul", "Sef", "Alex", "Paul"];

// //returns the first index at which a given element can be found
// console.log(tuitt.indexOf("Sef")); // 2

// //finds the first instances of Paul 1 not 4
// console.log(tuitt.indexOf("Paul")); // 1

// //returns -1 if the element is not present.
// console.log(tuitt.indexOf("Hulk")); // -1

// let colors = ["red", "orange", "yellow", "green"];
// // for( let i = 0; i < colors.length; i++)
// // {
// // 	console.log(`${colors[i]} is in ${i}`);
// // }

// colors.forEach(function(color){
// 	// color is placeholder
// 	console.log(`${color} is in ${colors.indexOf(color)}`);
// });


// const colors = ['red', 'green', 'blue'];
// // address 23 new st.

// const colors2 = ['red', 'green', 'blue'];
// // address 167 times new roman st.

// const colors3 = colors;

// const scores = [
// [12, 15, 16],
// [11, 9, 8],
// [1, 5, 6],
// ];
// scores.forEach(function(score){
// 	// color is placeholder
// 	console.log(score);
// });

// for(let i = 0; i < scores.length; i++){
// 	// console.log(scores[i]);
// 	// another loop for scores[i]
// 	for(let j = 0; j < scores[i].length; j++){
// 		console.log(scores[i][j])
// 	}
// }

// *** OBJECTS ***

const grades = [98, 87, 91]

const myGrades = {
lastName: 'Zamora',
firstName: 'Rie',
programming: 84,
math: 87,
english: 91,
science: 98,
hobbies:['coding', 'writing', 'cosplay']
}

const person = {
	firstName: 'John',
	lastName: 'Smith',
	email: ['jsmith@gmail.com',
			'js@gmail.com'],
	address: {
		city: 'Tokyo',
		country: 'Japan'
	},
	greeting: function(){
		return 'hi';
	}
}

const shoppingCart = [
	{
		product: 'shirt',
		price: 99.90,
		qty: 2
	},
	{
		product: 'watch',
		price: 9.90,
		qty: 1
	},
	{
		product: 'laptop',
		price: 999.90,
		qty: 2
	},
]


const blogger = {
	lname: 'Smith',
	fname: 'john',
	posts: [
		{
			title: 'my first post',
			body: 'lorem ipsum',
			comments:[
				{}
			]
		},
		{},
		{}
	]
}